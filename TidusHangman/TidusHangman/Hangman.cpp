 
#include <string>
#include "HelpfunctionsHangman.h"
#include "HangmanEnums.h" 
#include <Windows.h>
using std::cout;
#define ALPHABET 26



int main()
{	
	int Lose = 0;
	int life = 8;
	char input;
	char inputLetters[ALPHABET]{ 0 };
	Spielmodus spielmodus = Spielmodus::Default;
	nextgame NEXTGAME = nextgame::Standart;
	string hangmanWord = "";
	int userModeInput;
	char* rightLetters = nullptr; //Pointer  -> Der zeigt auf einer Adresse, aber zu dem Zeitpunkt auf keine

	while (NEXTGAME == Standart || NEXTGAME == Nextgame)
	{
		spielmodus = Spielmodus::Default;
		life = 8;
		for (int currentindex = 0 ; currentindex < ALPHABET-1; currentindex++)
		{
			inputLetters[currentindex] = 0;
		
		}

	    cout << "Spieler vs KI [1]  |  Spieler vs Spieler [2] " << endl;

		do
		{
			cin >> *(int*)&spielmodus;
			if (spielmodus == SvsS)
			{
				cout << "Spielmodus Spieler vs Spieler gewaehlt" << endl;
				cout << "Geben sie Ihr Wort ein !";
				cin >> hangmanWord;
				rightLetters = new char[hangmanWord.length()];  //Point wird zugewiesen (zeigt nun) auf die Adresse der ersten Schubalde des neuen Arrays
			}
			else if (spielmodus == SvsKI)
			{
				cout << "Spielmodus Spieler vs KI gewaehlt" << endl;
				hangmanWord = GetRandomWordOfFile("randomWords.txt");
				rightLetters = new char[hangmanWord.length()];
				Clear();
			}
			else
			{
				cout << "Falscher Modus" << endl;
			}
		} while (spielmodus >= 3);

		if (rightLetters == nullptr)
		{
			return EXIT_FAILURE;
		}

		InitArray(rightLetters, hangmanWord.length(), '_');
		cout << "Geben sie Ihr Buchstabe ein !" << endl;

		while (life > 0)
		{
			PrintArray(rightLetters, hangmanWord.length(), "ResultWord");
			PrintArray(inputLetters, ALPHABET, "InputLetters");
			cout << "Leben : " << life << endl;
			cout << "\nGeben sie Ihr Buchstabe ein !" << endl;
			cin >> input;
			if (!IsInputLetter(input)) continue;
			input = ToLower(input);
			Clear();

			if (!AddNewLetterToArray(input, inputLetters))
			{
				cout << "Eingegebener Buchstabe schonmal eingegeben " << endl;
				continue;
			}
			int offset = 0;
			bool found = false;
			while (IsLetterInArray(input, hangmanWord, hangmanWord.length(), offset))
			{
				found = true;
				rightLetters[GetLetterPos(input, hangmanWord, hangmanWord.length(), offset)] = input;
				offset++;
			}
			if (found)
			{
				cout << "Richtig" << endl;
			}
			else
			{
				cout << "falsch" << endl;
				life--;
			}
			if (IsWin(rightLetters, hangmanWord.length()))
			{
				cout << "WIN" << endl;
				Sleep(2000);
				Clear();
				Lose = 1;
				break;
			}
			if (life <= 0)
			{
				cout << "Verloren" << endl;
				cout << "Gesuchtes Wort : " << hangmanWord << endl;
			}
		}
	}
}
 