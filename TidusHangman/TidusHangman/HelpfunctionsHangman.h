#include <iostream>
#include <fstream>
#include <time.h>
using namespace std;

void PrintArray(char arr[], int size, string outputInfo)
{
	cout << outputInfo + ": ";
	for (int currentindex = 0; currentindex < size; currentindex++)
	{
		cout << arr[currentindex] << " ";
	}
	cout << endl;
}

void InitArray(char* arr, int size, char zeichen)
{
	for (int currentindex = 0; currentindex < size; currentindex++)
	{
		arr[currentindex] = zeichen;
	}
}

bool IsWin(char arr[], int size)
{
	for (int currentindex = 0; currentindex < size; currentindex++)
	{
		if (arr[currentindex] == '_') return false;
	}
	return true;
}

char ToLower(char input)
{
	if (input >= 'A' && input <= 'Z')
	{
		input = input + 32;
	}
	return input;

}

bool IsInputLetter(char input)
{
	bool isLetter = input >= 'A' && input <= 'Z' || input >= 'a' && input <= 'z';
	system("cls");
	if(!isLetter) cout << "Eingabe ist kein Zeichen" << endl;
	
	return isLetter;
}


bool IsLetterInArray(char letter, char* LetterArray, int ArraySize) 
{
	for (int currentindex = 0; currentindex < ArraySize; currentindex++)
	{
		if (ToLower(LetterArray[currentindex]) == letter)
		{
			return  true;
		}
	}
	return false;
}

bool IsLetterInArray(char letter, string LetterArray, int ArraySize, int start) // oder hier 
{
	for (int currentindex = start; currentindex < ArraySize; currentindex++)
	{
		if (ToLower(LetterArray[currentindex]) == letter)
		{
			return true;
		}
	}
	return false;
}
int GetLetterPos (char letter , string arr, int arrsize , int start )
{
	for (int currentindex = start; currentindex < arrsize; currentindex++)
	{
		if (ToLower(arr[currentindex]) == letter)
		{
			return currentindex;
		}
	}

	return -1;
}

bool AddNewLetterToArray(char letter, char* array)
{
	if (int(array[letter - 'a']) == letter) return false;
	array[letter - 'a'] = letter;
	return true;
}

void Clear(void)
{
	system("cls");
}


int getFileLength(string fileName)
{
	int length = 0;
	string tempWord = "";
	ifstream geleseneDatei(fileName);

	if (!geleseneDatei.is_open())
	{
		cerr << "Datei: " << fileName << " konnte nicht gelesen werden!" << endl;
		return -1;
	}

	while (geleseneDatei >> tempWord) length++;
	geleseneDatei.close();
	return length;
}
int getRandomNumber(int range)
{
	srand(time(NULL));
	return rand() % range;
}
string GetRandomWordOfFile(string fileName)
{
	int FileLength = getFileLength(fileName);
	string* readWords = new string[FileLength];
	string currentWord = "";
	size_t currentindex = 0;
	ifstream geleseneDatei(fileName);

	if (!geleseneDatei.is_open())
	{
		cerr << "Datei: " << fileName << " konnte nicht gelesen werden!" << endl;
		return "";
	}

	if (readWords == NULL) cout << "NULL Pointer" << endl;

	while (geleseneDatei >> currentWord)
	{
		readWords[currentindex++] = currentWord;
	}

	geleseneDatei.close();
	return readWords[getRandomNumber(FileLength)];
}

